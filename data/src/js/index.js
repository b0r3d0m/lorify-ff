'use strict';

const notifications = require('sdk/notifications');
function showNotification(title, text, iconURL, onClick) {
  notifications.notify({
    title: title,
    text: text,
    iconURL: iconURL,
    onClick: onClick
  });
}

const pageMod = require('sdk/page-mod');
pageMod.PageMod({
  include: [
    /https?:\/\/www.linux.org.ru\/forum\/.*/,
    /https?:\/\/www.linux.org.ru\/gallery\/.*/,
    /https?:\/\/www.linux.org.ru\/news\/.*/,
    /https?:\/\/www.linux.org.ru\/polls\/.*/
  ],
  contentScriptFile: [
    './src/libs/jquery/jquery-2.2.0.min.js',
    './src/js/contentscript.js'
  ],
  // LOR's threads contains iframes, and
  // Firefox will attach content scripts to iframes by default
  attachTo: ['existing', 'top'],
  onAttach: function(worker) {
    const tab = worker.tab;

    worker.port.on('showNotification', function(options) {
      showNotification(
        options.title,
        options.text,
        options.iconURL,
        function(data) {
          tab.activate();
      });
    });

    worker.port.on('getPrefs', function() {
      const prefs = require('sdk/simple-prefs').prefs;
      worker.port.emit('onPrefs', prefs);
    });
  }
});
